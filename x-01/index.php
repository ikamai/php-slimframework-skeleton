<?php
require 'vendor/autoload.php';



use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

// init
$config = ['settings' => [
    'displayErrorDetails' => true,
    'addContentLengthHeader' => true,
]];
$app = new \Slim\App($config);


/**
 * dependency injection
 */
$container = $app->getContainer();
$container['sempak'] = function ($c) {
    return 'sempak is gud';
};
//$env = $container['environment'];

// Register component on container
$container['view'] = function ($container) {
    $view = new \Slim\Views\Twig(__DIR__ . '/theme', [
        //'cache' => __DIR__ . '/cache/'
    ]);

    // Instantiate and add Slim specific extension
    $router = $container->get('router');
    $uri = \Slim\Http\Uri::createFromEnvironment(new \Slim\Http\Environment($_SERVER));
    $view->addExtension(new Slim\Views\TwigExtension($router, $uri));

    return $view;
};


/**
 * Middleware
 */

/*
$app->add(function ($request, $response, $next) {
    $response->getBody()->write('BEFORE');
    $response = $next($request, $response);
    $response->getBody()->write('AFTER');

    return $response;
});
 */

/**
 * routing
 */
$app->options('/{routes:.+}', function ($request, $response, $args) {
    return $response;
});

$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
        ->withHeader('Access-Control-Allow-Origin', '*')
        ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
        ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
});


$app->get('/', function (Request $request, Response $response, array $args) {
    
    // load dependency
    $ts = $this->sempak . ' - -';
    //print_r($this->environment['slim.files']);


    return $this->view->render(
        $response,
        "index.twig",
        ['data' => $ts]
    );

    // with middleware
    $response->getBody()->write("Hello, $ts");
    return $response;
});

// get json data
$app->post('/json', function (Request $request, Response $response, array $args) {
    echo '<pre>';
    // get json data: arr
    $parsedBody = $request->getParsedBody();
    print_r($parsedBody);
});

$app->get('/req', function (Request $request, Response $response, array $args) {
    echo '<pre>';
    print_r($request->isGet());
    print_r($request->getMethod());
    print_r($request->getUri());

    $headers = $request->getHeaders();
    foreach ($headers as $name => $values) {
        echo $name . ": " . implode(", ", $values) . "\n";
    }


    echo '</pre>';
});
// another way dependency
$app->get('/dep', function (Request $request, Response $response, array $args) {
    if ($this->has('sempak')) {
        $ts = '-' . $this->get('sempak') . '-'; //$this->sempak;
    } else {
        $ts = 'no ts';
    }

    $response->getBody()->write(" Hello, $ts ");
    return $response;
});

// 400
$app->get('/papat', function ($request, $response, $args) {
    return $response->withStatus(404)->write('Hello World!');
});

// put
$app->put('/books/{id}', function ($request, $response, $args) {
    // Update book identified by $args['id']
});

// any
$app->any('/cook/{id}', function ($request, $response, $args) {
    // Apply changes to books or book identified by $args['id'] if specified.
    // To check which method is used: $request->getMethod();
});

// set cookie
$app->get('/hello/{name}', function ($request, $response, $args) {

});

// get settings
$app->get('/settings', function ($request, $response, $args) {

    $datane = (array)$this->get('settings');

    $response = $response
        ->withAddedHeader('Access-Control-Allow-Methods', 'POST, GET, OPTIONS')
        ->withAddedHeader('Access-Control-Allow-Origin', '*');
    $r = $response->withJson($datane);
    return $r;
});

$app->post('/new', function (Request $request, Response $response) {

    $data = $request->getParsedBody();

    $datane = [];
    $datane['title'] = filter_var($data['title'], FILTER_SANITIZE_STRING);
    $datane['description'] = filter_var($data['description'], FILTER_SANITIZE_STRING);

    $response = $response
        ->withAddedHeader('Access-Control-Allow-Methods', 'POST, GET, OPTIONS')
        ->withAddedHeader('Access-Control-Allow-Origin', '*');
    $r = $response->withJson($datane);
    return $r;
});

$app->map(['GET', 'POST', 'PUT', 'DELETE', 'PATCH'], '/{routes:.+}', function ($req, $res) {
    $handler = $this->notFoundHandler; // handle using the default Slim page not found handler
    return $handler($req, $res);
});

// runner
$app->run();
