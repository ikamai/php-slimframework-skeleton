<?php

/**
 * Versi Ke-2 Skeleton
 * 
 */
date_default_timezone_set('America/New_York');

require __DIR__ . '/../vendor/autoload.php';

// load env
$dotenv = new Dotenv\Dotenv(__DIR__ . '/../');
$dotenv->load();

require __DIR__ . '/../app/config/settings.php';

$app = new \Slim\App($config);

require __DIR__ . '/../app/config/dependencies.php';
require __DIR__ . '/../app/config/middleware.php';
require __DIR__ . '/../app/config/routes.php';

// runner
$app->run();
