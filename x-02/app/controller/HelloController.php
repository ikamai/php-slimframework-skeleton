<?php
namespace App\Controller;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

class HelloController // extends BaseController
{
    protected $helloModel;

    public function __construct($helloModel)
    {
        $this->helloModel = $helloModel;
    }

    public function index(Request $request, Response $response, array $args)
    {
        // load dependency
        // $ts = $this->sempak . ' - -';
        //print_r($this->environment['slim.files']);

        /*
        return $this->view->render(
            $response,
            "index.twig",
            ['data' => $ts]
        );
         */
        $e = $this->helloModel->kopet();
        $response->getBody()->write("Hello, $e");
        return $response;
    }


    public function json(Request $request, Response $response, array $args)
    {

        $response->getBody()->write("Hello");
        return $response;
    }

    public function req(Request $request, Response $response, array $args)
    {
        echo '<pre>';
        print_r($request->isGet());
        print_r($request->getMethod());
        print_r($request->getUri());

        $headers = $request->getHeaders();
        foreach ($headers as $name => $values) {
            echo $name . ": " . implode(", ", $values) . "\n";
        }
        echo '</pre>';
    }

    public function dep(Request $request, Response $response, array $args)
    {
        // not working 
        if ($this->has('sempak')) {
            $ts = '-' . $this->get('sempak') . '-'; //$this->sempak;
        } else {
            $ts = 'no ts';
        }

        $response->getBody()->write(" Hello, $ts ");
        return $response;
    }

    public function notfound(Request $request, Response $response, array $args)
    {
        return $response->withStatus(404)->write('Hello World!');
    }

    public function parsedbody(Request $request, Response $response, array $args)
    {
        $data = $request->getParsedBody();

        $datane = [];
        $datane['title'] = filter_var($data['title'], FILTER_SANITIZE_STRING);
        $datane['description'] = filter_var($data['description'], FILTER_SANITIZE_STRING);

        $response = $response
            ->withAddedHeader('Access-Control-Allow-Methods', 'POST, GET, OPTIONS')
            ->withAddedHeader('Access-Control-Allow-Origin', '*');
        $r = $response->withJson($datane);
        return $r;
    }

    public function settings(Request $request, Response $response, array $args)
    {
        //notworking
        $datane = (array)$this->get('settings');

        $response = $response
            ->withAddedHeader('Access-Control-Allow-Methods', 'POST, GET, OPTIONS')
            ->withAddedHeader('Access-Control-Allow-Origin', '*');
        $r = $response->withJson($datane);
        return $r;
    }
}