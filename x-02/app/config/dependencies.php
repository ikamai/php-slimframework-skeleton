<?php

/**
 * dependency injection
 */

$container = $app->getContainer();
$container['sempak'] = function ($c) {
    return 'sempak is gud';
};
//$env = $container['environment'];

// Register component on container
$container['view'] = function ($container) {
    $view = new \Slim\Views\Twig(__DIR__ . '/../../themes/', [
        //'cache' => __DIR__ . '/cache/'
    ]);
    $router = $container->get('router');
    $uri = \Slim\Http\Uri::createFromEnvironment(new \Slim\Http\Environment($_SERVER));
    $view->addExtension(new Slim\Views\TwigExtension($router, $uri));
    return $view;

    //$basePath = rtrim(str_ireplace('index.php', '', $c['request']->getUri()->getBasePath()), '/');
    //$view->addExtension(new \Slim\Views\TwigExtension($c['router'], $basePath));
    //return $view;

};

//Override the default Not Found Handler
$container['notFoundHandler'] = function ($c) {
    return function ($request, $response) use ($c) {
        return $c['response']
            ->withStatus(404)
            ->withHeader('Content-Type', 'text/html')
            ->write('404 Dude!');
    };
};

$container['notAllowedHandler'] = function ($c) {
    return function ($request, $response) use ($c) {
        return $c['response']
            ->withStatus(404)
            ->withHeader('Content-Type', 'text/html')
            ->write('Dude! this page is Forbidden');
    };
};

$container['errorHandler'] = function ($c) {
    return function ($request, $response, $exception) use ($c) {
        return $c['response']->withStatus(503)
            ->withHeader('Content-Type', 'text/html')
            ->write('Maintenance - Come back in 2 minutes');
    };
};


/**
 * Models
 */
$container['HelloModel'] = function ($c) {
    return new App\Model\HelloModel();
};

/**
 * Controller
 */
$container[App\Controller\HelloController::class] = function ($c) {
    return new App\Controller\HelloController($c['HelloModel']);
};

