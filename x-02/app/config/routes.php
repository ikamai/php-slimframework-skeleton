<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

/**
 * routing
 */
$app->options('/{routes:.+}', function ($request, $response, $args) {
    return $response;
});

$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
        ->withHeader('Access-Control-Allow-Origin', '*')
        ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
        ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
});


$app->get('/', 'App\Controller\HelloController:index');
// get json data
$app->post('/json', 'App\Controller\HelloController:json');
$app->get('/req', 'App\Controller\HelloController:req');
// another way dependency
$app->get('/dep', 'App\Controller\HelloController:dep');
// 400
$app->get('/notfound', 'App\Controller\HelloController:notfound');
// put
$app->put('/books/{id}', 'App\Controller\HelloController:notfound');
// any
$app->any('/cook/{id}', 'App\Controller\HelloController:notfound');

// get settings
$app->get('/settings', 'App\Controller\HelloController:settings');

$app->post('/parsedbody', 'App\Controller\HelloController:parsedbody');

$app->map(['GET', 'POST', 'PUT', 'DELETE', 'PATCH'], '/{routes:.+}', function ($req, $res) {
    $handler = $this->notFoundHandler; // handle using the default Slim page not found handler
    return $handler($req, $res);
});